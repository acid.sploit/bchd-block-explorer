https://explorer.sploit.cash

# bchd-block-explorer

A BCHD gRPC block explorer

## Init log

```
yarn create react-app bchd-block-explorer
```

### gRPC protobuf
```
yarn add ts-protoc-gen
yarn add @improbable-eng/grpc-web
yarn add @types/google-protobuf
yarn add google-protobuf

```

#### Generate JS libs

* Download the `.proto` file from GitHub: [bchrpc.proto](https://github.com/gcash/bchd/blob/master/bchrpc/bchrpc.proto)
* Generate the libs with `protoc`


```
cd src/
wget https://raw.githubusercontent.com/gcash/bchd/master/bchrpc/bchrpc.proto

mkdir bchrpc
protoc \
  --plugin=protoc-gen-ts=../node_modules/.bin/protoc-gen-ts \
  --js_out=import_style=commonjs,binary:bchrpc \
  --ts_out=service=grpc-web:bchrpc \
  ./bchrpc.proto
```

Finally add `/* eslint-disable */` at the top of the generated .js libs.
* bchrpc/bchrpc_pb_service.js
* bchrpc/bchrpc_pb.js

### Materialize-css
```
yarn add react-materialize
```